#include <vector>
#include <memory>
#include <iostream>
#include <sstream>


#include "board.h"
#include "piece.h"

namespace cs427_527
{
  Board::Board(int w, int h):  
  width(w),
  height(h),
  turn(0)
  {
  }

  Board::~Board()
  {
  }


  int Board::getWidth() const
  {
    return width;
  }
  int Board::getHeight() const
  {
    return height;
  }

  int Board::getCurrentPlayer() const
  {
    return turn;
  }

  bool Board::inBounds(int r, int c) const
  {
    return r >= 0 && r < height && c >= 0 && c < width;
  }

  

}