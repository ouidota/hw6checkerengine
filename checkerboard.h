#ifndef __CHECKERBOARD_H__
#define __CHECKERBOARD_H__

#include <string>
#include <iostream>
#include <vector>
#include <memory>
#include "board.h"

namespace cs427_527
{
  class Checker;

  class CheckerBoard : public Board
  {
  public:
    enum Color { BLACK, RED };
    Color getBoardColor(int row, int col) const;
    std::shared_ptr<Checker> getPiece(int r, int c);
    std::shared_ptr<Checker> getPiece(int r, int c) const;
    void placePiece(int r, int c, std::shared_ptr<Checker> p);


  
    CheckerBoard();
    virtual ~CheckerBoard();
    void removePiece(int r, int c) override;
    bool isLegalMove(int fromR, int fromC, int toR, int toC) const override;  
    void makeMove(int fromR, int fromC, int toR, int toC) override;
    bool gameOver() const override;

    std::string toString() const override ;
 

  protected:
 
    int findLongestMove() const;
    int findLongestMove(int fromR, int fromC) const;
    std::shared_ptr<Checker> jumping;
    std::vector<std::vector<std::shared_ptr<Checker>>> board;

  };

  std::ostream& operator<<(std::ostream& os, const CheckerBoard& board);
}
  
#endif

