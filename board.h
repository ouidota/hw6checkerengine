#ifndef __BOARD_H__
#define __BOARD_H__

#include <string>
#include <iostream>
#include <vector>
#include <memory>
#include "piece.h"


namespace cs427_527
{
    class Checker;
    class Board
    {
    public: 
        // enum Color {BLACK, RED};
        Board(int w, int h);

        virtual ~Board();

        virtual void removePiece(int r, int c) = 0;
        int getWidth() const;
        int getHeight() const;
        int getCurrentPlayer() const;
        bool inBounds(int r, int c) const;



        virtual bool isLegalMove(int fromR, int fromC, int toR, int toC) const = 0;
        virtual void makeMove(int fromR, int fromC, int toR, int toC) = 0;
        virtual bool gameOver() const = 0;
        virtual std::string toString() const = 0;

    protected:
        int width;
        int height;
        int turn;
        
        // std::vector<std::vector<std::shared_ptr<Piece>>> board;


    };




}



#endif