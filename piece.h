#ifndef __PIECE_H__
#define __PIECE_H__

#include <string>
#include <memory>
#include <vector>

namespace cs427_527
{
  class Board;

  class Piece
  {
  public:
    Piece(int p, int r, int c);
    virtual ~Piece();

    int getPlayer() const;

    // virtual bool isLegalMove(const Board& board, int toR, int toC) const = 0;
    // virtual void makeMove(Board& board, int toR, int toC) = 0;
    virtual std::string toString() const = 0;

  protected:
    int player;    
    int row;
    int col;
    // std::vector<std::string> printer;

  };
}





#endif