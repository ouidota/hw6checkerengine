#ifndef __CHECKER_H__
#define __CHECKER_H__

#include <string>
#include <memory>
#include <vector>
#include "piece.h"
#include "checkerboard.h"


namespace cs427_527
{
  class CheckerBoard;

  /**
   * A checker.
   */
  class Checker : public Piece
  {
  public:

    Checker(int p, int r, int c);
    virtual ~Checker();

    bool isLegalMove(const CheckerBoard& board, int toR, int toC) const ;
    void makeMove(CheckerBoard& board, int toR, int toC) ;
    std::string toString() const override;


    
  protected:

    virtual bool isLegalDestination(const CheckerBoard& board, int toR, int toC) const;
  
    virtual bool isLegalDirection(int toR, int toC) const;
  
    virtual bool isLegalDistance(int dist) const;

    virtual bool isLegalJump(const CheckerBoard& board, int toR, int toC) const;

    virtual bool canMoveBackwards() const;
   
    virtual void jump(CheckerBoard& board, int toR, int toC) const;

    virtual bool checkPromote(const CheckerBoard& board, int toR, int toC) const;

    virtual std::shared_ptr<Checker> promote() const;

  };
}

#endif
