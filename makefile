CXX=g++
CXXFLAGS=-Wall -pedantic -std=c++11 -g3

Checkers: checkers.o board.o checkerboard.o piece.o checker.o king.o math.o
	${CXX} -o $@ ${CXXFLAGS} $^
